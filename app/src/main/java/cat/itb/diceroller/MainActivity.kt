package cat.itb.diceroller

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast

import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    lateinit var buttonRoller: Button
    lateinit var buttonReset: Button
    lateinit var  dice: ImageButton
    lateinit var  dice2: ImageButton
    lateinit var mediaPlayer: MediaPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* Assing */
        dice = findViewById(R.id.result)
        dice2 = findViewById(R.id.result2)
        val myToast = Toast.makeText(applicationContext,"JACKPOT!", Toast.LENGTH_SHORT)
        myToast.setGravity(Gravity.CENTER_HORIZONTAL,0,0)

        var resultat = 0
        var resultat2 = 0

        buttonRoller = findViewById(R.id.ButtonRoller)
        buttonReset = findViewById(R.id.ButtonReset)

        mediaPlayer = MediaPlayer.create(this, R.raw.clic)

        buttonRoller.setOnClickListener(){
            mediaPlayer.start()
            resultat = Roller()
            resultat2 = Roller()

            DiceChanger(dice, resultat)
            DiceChanger(dice2, resultat2)



            if (resultat==6 && resultat2==6){
                myToast.show()
            }
        }

        buttonReset.setOnClickListener(){
            dice.setImageResource(R.drawable.empty_dice)
            dice2.setImageResource(R.drawable.empty_dice)

        }

        dice.setOnClickListener(){
            mediaPlayer.start()
            DiceChanger(dice, Roller())


        }

        dice2.setOnClickListener(){
            mediaPlayer.start()
            DiceChanger(dice2, Roller())

        }


    }

    private fun Roller(): Int {
        val randomValues = Random.nextInt(1, 7)
        return randomValues
    }

    private fun DiceChanger(button: ImageButton, x: Int){

        when (x) {
            1 -> {
                button.setImageResource(R.drawable.dice_1)
            }
            2 -> {
                button.setImageResource(R.drawable.dice_2)
            }
            3 -> {
                button.setImageResource(R.drawable.dice_3)
            }
            4 -> {
                button.setImageResource(R.drawable.dice_4)
            }
            5 -> {
                button.setImageResource(R.drawable.dice_5)
            }
            6 -> {
                button.setImageResource(R.drawable.dice_6)
            }

        }

    }
}